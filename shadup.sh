#!/bin/bash 

version=0.4
cyan=$(tput setaf 6)
res=$(tput sgr0)

if [[ $1 == "-h" ||  $1 == "--help" ]]; then
    echo "The program shadup finds files duplicated by sha1sum."
    echo "Options:"		
		echo -e "\t--version: output version information and exit."
    exit 0

elif [[ $1 == "--version" ]]; then
	echo -e "\n${cyan}shadup version:${res-} $version beta (https://github.com/roberseik)"
	exit 0	
fi		


start=$(date +%s)

find -type f -exec sha1sum {} \; | sort | uniq -w42 -D --all-repeated=separate  > duplicate.txt &

pid=$! # Process Id 
while kill -0 $pid 2>/dev/null; do
 	echo -ne "\rAnalyzing files.  "
	sleep 0.5
	echo -ne "\rAnalyzing files.. "
	sleep 0.5
	echo -ne "\rAnalyzing files..."
	sleep 0.5
done

sed -i '/^$/d' duplicate.txt 

finish=$(date +%s)

echo "Text files duplicated:" $(wc -l < duplicate.txt)
echo "Text file created: duplicate.txt "
echo "Execution time: $((finish - start)) secs."

